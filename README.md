# Amdocs Hands-On Exercise Plain JavaScript Version (only back-end)

## Running

- You need NodeJs 12.x installed, you can obtain here [NodeJS](https://nodejs.org/en/).

### On Production

#### 1 - Build the application:

- Run ``npm install --prod``
- Congratulations, the build is complete :)

#### 2 - Execute

- Linux:
  - Run ``npm run start``
- Windows:
  - Run ``npm run start_windows``
- Or use [PM2](https://pm2.io/docs/plus/overview/) to manage NodeJS process.  
