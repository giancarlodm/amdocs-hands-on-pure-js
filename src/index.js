require("./load-modules");

const http = require("http");

const compression = require("compression");
const express = require("express");
const rateLimit = require("express-rate-limit");

const errorHandlerAppMiddleware = require("./core/http/app-middlewares/error-handler.app-middleware");
const apiRouter = require("./api-router");

const app = express();

// production
if (process.env.NODE_ENV === "production") {

	// Rate limiting
	app.use(rateLimit({
		windowMs: 1000 * 60, // 1 minute
		max: 100, // 100 requests
		message: "Too Many Requests!",
		headers: true
	}));

	// Compression with zlib
	app.use(compression());
}

// Application Middleware to parse requests with Content-Type application/json
app.use(express.json());

// Disable the "X-Powered-By" Header on responses, avoiding giving hackers/crackers clues of
// what tech is being used
app.set("x-powered-by", false);

// API Routes
app.use("/api", apiRouter);

// Default Error Handler
app.use(errorHandlerAppMiddleware);

const httpServer = http.createServer(app);
httpServer.listen(process.env.PORT, () => {
	console.log(`\tServer is listening on port: ${3000} and on HTTP mode`);
});

/**
 * Configures "SIGINT" to allow "Graceful Shutdown".
 */
process.on("SIGINT", () => {

	httpServer.close((error) => {

		if (error != null) {
			console.error(error);
			process.exit(1);
		}
		else {
			process.exit(0);
		}
	});
});
