const express = require("express");

const requestValidatorMiddlewareFactory = require("../../../core/schema-validation/middlewares/request-validator.middleware");
const HttpSuccessStatusEnum = require("../../../core/http/status/http-success-status.enum");
const apiRouter = require("../../../api-router");
const Customer = require("../../shared/entities/customer");
const CustomersService = require("../../shared/services/customers.service");
const requestSchemas = require("../request-schemas/customers");

const customerRouter = express.Router();

/**
 * Retrieves the list of customers.
 * TODO if pagination is required, an object with pagination metadata (total, limit, skip) must
 *  be returned instead of a plain Array
 * @param res Response object.
 * @param queryParams Query parameters.
 */
customerRouter.get(
	"/",
	requestValidatorMiddlewareFactory(requestSchemas.findRequestSchema),
	async (req, res, next) => {

		try {
			const customers = await CustomersService.find(req.query);
			res.status(HttpSuccessStatusEnum.OK).json(customers);
		}
		catch (e) {
			next(e);
		}
	}
);

/**
 * Inserts a customer. Returns the generated id.
 * @param res Response object.
 * @param body Request body with the customer to insert.
 */
customerRouter.post(
	"/",
	requestValidatorMiddlewareFactory(requestSchemas.insertOneRequestSchema),
	async (req, res, next) => {

		const customer = new Customer();
		customer.id = undefined;
		customer.name = req.body.name;
		customer.situation = req.body.situation;

		try {
			await CustomersService.save(customer);
			res.status(HttpSuccessStatusEnum.CREATED).json({id: customer.id});
		}
		catch (e) {
			next(e);
		}
	}
);

/**
 * Replaces an customer.
 * @param res Response object.
 * @param id The id of the customer to replace.
 * @param body The request body with the customer replacement.
 */
customerRouter.put(
	"/:id",
	requestValidatorMiddlewareFactory(requestSchemas.replaceOneByIdRequestSchema),
	async (req, res, next) => {

		const customer = new Customer();
		customer.id = req.params.id;
		customer.name = req.body.name;
		customer.situation = req.body.situation;

		try {
			await CustomersService.save(customer);
			res.sendStatus(HttpSuccessStatusEnum.NO_CONTENT);
		}
		catch (e) {
			next(e);
		}
	}
);

/**
 * Performs a partial update on the customer.
 * @param res Response object.
 * @param id The id of the customer to partialy update.
 * @param body The request body with the update to perform.
 */
customerRouter.patch(
	"/:id",
	requestValidatorMiddlewareFactory(requestSchemas.patchOneByIdRequestSchema),
	async (req, res, next) => {

		try {
			await CustomersService.patchOneById(req.params.id, req.body);
			res.sendStatus(HttpSuccessStatusEnum.NO_CONTENT);
		}
		catch (e) {
			next(e);
		}
	}
);

/**
 * Removes a customer.
 * @param res Response Object
 * @param id The id of the customer to delete.
 */
customerRouter.delete(
	"/:id",
	requestValidatorMiddlewareFactory(requestSchemas.removeOneByIdRequestSchema),
	async (req, res, next) => {

		try {
			await CustomersService.removeOneById(req.params.id);
			res.sendStatus(HttpSuccessStatusEnum.NO_CONTENT);
		}
		catch (e) {
			next(e);
		}
	}
);

apiRouter.use("/customers", customerRouter);
