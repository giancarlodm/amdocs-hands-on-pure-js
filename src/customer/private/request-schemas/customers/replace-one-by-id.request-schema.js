const ConvenienceMethods = require("../../../../core/common/convenience-methods.class");
const CustomerSituationEnum = require("../../../shared/enums/customer-situation.enum");

/**
 * JSON Schemas for {@link customerRouter.put("/:id")}.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
const replaceOneByIdRequestSchema = {

    params: {
        type: "object",
        required: ["id"],
        additionalProperties: false,
        properties: {
            id: {
                type: ["null", "integer"],
                minimum: 0
            }
        }
    },

    body: {
        type: "object",
        required: ["name"],
        additionalProperties: false,
        properties: {
            // Id is accepted, but the one informed on the URL prevails
            id: {
                type: ["null", "integer"]
            },
            name: {
                type: "string",
                maxLength: 500
            },
            situation: {
                type: "string",
                enum: ConvenienceMethods.getEnumeratorValues(CustomerSituationEnum)
            }
        }
    }
};

module.exports = replaceOneByIdRequestSchema;
