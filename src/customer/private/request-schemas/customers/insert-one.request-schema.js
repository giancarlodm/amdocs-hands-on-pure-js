const ConvenienceMethods = require("../../../../core/common/convenience-methods.class");
const CustomerSituationEnum = require("../../../shared/enums/customer-situation.enum");

/**
 * JSON Schemas for {@link customerRouter.post("/")}.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
const insertOneRequestSchema = {

    body: {
        type: "object",
        required: ["name", "situation"],
        additionalProperties: false,
        properties: {
            name: {
                type: "string",
                maxLength: 500
            },
            situation: {
                type: "string",
                enum: ConvenienceMethods.getEnumeratorValues(CustomerSituationEnum)
            }
        }
    }
};

module.exports = insertOneRequestSchema;
