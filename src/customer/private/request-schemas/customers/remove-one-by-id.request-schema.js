/**
 * JSON Schemas for {@link customerRouter.delete("/:id")}.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
const removeOneByIdRequestSchema = {

    params: {
        type: "object",
        required: ["id"],
        additionalProperties: false,
        properties: {
            id: {
                type: ["null", "integer"],
                minimum: 1
            }
        }
    }
};

module.exports = removeOneByIdRequestSchema;
