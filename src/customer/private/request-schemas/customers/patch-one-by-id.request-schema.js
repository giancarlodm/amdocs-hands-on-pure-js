const ConvenienceMethods = require("../../../../core/common/convenience-methods.class");
const CustomerSituationEnum = require("../../../shared/enums/customer-situation.enum");

/**
 * JSON Schemas for {@link customerRouter.patch("/:id")}.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
const patchOneByIdRequestSchema = {

    params: {
        type: "object",
        required: ["id"],
        additionalProperties: false,
        properties: {
            id: {
                type: ["null", "integer"],
                minimum: 0
            }
        }
    },

    body: {
        type: "object",
        additionalProperties: false,
        properties: {
            name: {
                type: "string",
                maxLength: 500
            },
            situation: {
                type: "string",
                enum: ConvenienceMethods.getEnumeratorValues(CustomerSituationEnum)
            }
        }
    }
};

module.exports = patchOneByIdRequestSchema;
