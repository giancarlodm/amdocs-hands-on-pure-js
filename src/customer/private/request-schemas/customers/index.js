module.exports.findRequestSchema = require("./find.request-schema");
module.exports.insertOneRequestSchema = require("./insert-one.request-schema");
module.exports.patchOneByIdRequestSchema = require("./patch-one-by-id.request-schema");
module.exports.removeOneByIdRequestSchema = require("./remove-one-by-id.request-schema");
module.exports.replaceOneByIdRequestSchema = require("./replace-one-by-id.request-schema");
