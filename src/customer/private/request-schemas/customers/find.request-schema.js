const ConvenienceMethods = require("../../../../core/common/convenience-methods.class");
const CustomerSituationEnum = require("../../../shared/enums/customer-situation.enum");

/**
 * JSON Schemas for {@link customerRouter.get("/")}.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
const findRequestSchema = {

    query: {
        type: "object",
        additionalProperties: false,
        properties: {
            filters: {
                type: "object",
                additionalProperties: false,
                properties: {
                    firstName: {
                        type: "string",
                        maxLength: 250
                    },
                    middleName: {
                        type: "string",
                        maxLength: 250
                    },
                    lastName: {
                        type: "string",
                        maxLength: 250
                    },
                    situation: {
                        type: "string",
                        enum: ConvenienceMethods.getEnumeratorValues(CustomerSituationEnum)
                    }
                }
            },
            options: {
                limit: {
                    type: "integer",
                    minimum: 0
                },
                skip: {
                    type: "integer",
                    minimum: 0
                }
            }
        }
    }
};

module.exports = findRequestSchema;
