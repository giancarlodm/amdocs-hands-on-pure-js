const EntityManager = require("../../../core/database/services/entity-manager.service");
const HttpNotFoundException = require( "../../../core/http/exceptions/client-errors/http-not-found-exception.class");
const Customer = require("../entities/customer");

/**
 * Business logic service for {@link Customer}.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
class CustomersService {


    /**
     * Retrieves the list of customers.
     * @param queryParams Parameters for conditions and pagination.
     * @return {Array<Customer>} List of customers
     */
    static async find(queryParams) {

        if (queryParams != null) {
            return EntityManager.find(Customer, queryParams.filters, queryParams.options);
        }
        else {
            return EntityManager.find(Customer);
        }
    }

    /**
     * Inserts or updates a customer. During insertion, the id is generated and set on the entity.
     * @param customer the customer to be saved.
     * @return {Customer} The customer saved. Same instance as the received parameter.
     */
    static async save(customer) {

        // An replace, check if customer exists
        if (customer.id != null) {

            const count = await EntityManager.count(Customer, {id: customer.id}, {limit: 1, skip: 0});
            if (count === 0) {
                throw new HttpNotFoundException("Customer not found", {
                    entity: Customer.name,
                    id: customer.id
                });
            }
        }

        return EntityManager.save(customer);
    }

    /**
     * Partially updates an customer.
     * @param id The id of the customer to patch.
     * @param partialCustomer The update to perform.
     */
    static async patchOneById(id, partialCustomer) {

        const modified = await EntityManager.update(Customer, {id: id}, partialCustomer);

        if (modified.modified === 0) {
            throw new HttpNotFoundException("Customer not found", {
                entity: Customer.name,
                id: id
            });
        }
    }

    /**
     * Removes a customer
     * @param id The id of the customer to remove
     */
    static async removeOneById(id) {

        const removed = await EntityManager.delete(Customer, {id: id});
        if (removed.removed === 0) {
            throw new HttpNotFoundException("Customer not found", {
                entity: Customer.name,
                id: id
            });
        }
    }
}

module.exports = CustomersService;
