/**
 * A customer.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
class Customer {

	//#region Public Attributes
	/**
	 * Customer's id.
	 * @type number
	 */
	id;
	/**
	 * Customer's name.
	 * @type string
	 */
	name;
	/**
	 * The customer situation.
	 * @type CustomerSituationEnum
	 */
	situation;
	//#endregion
}

module.exports = Customer;
