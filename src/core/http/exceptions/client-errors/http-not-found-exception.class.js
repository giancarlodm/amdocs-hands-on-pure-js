const HttpClientErrorStatusEnum = require("../../status/http-client-error-status.enum");
const HttpException = require("../http-exception.class");

/**
 * The 404 Not Found HTTP Exception.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
class HttpNotFoundException extends HttpException {

    constructor(message, details) {
        super(HttpClientErrorStatusEnum.NOT_FOUND, message, details);
    }
}

module.exports = HttpNotFoundException;
