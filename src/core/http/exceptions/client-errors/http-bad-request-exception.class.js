const HttpClientErrorStatusEnum = require("../../status/http-client-error-status.enum");
const HttpException = require( "../http-exception.class");

/**
 * The 400 Bad Request HTTP exception.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
class HttpBadRequestException extends HttpException {

    /**
     * constructor();
     * constructor(message: string);
     * constructor(message: string, details: T);
     */
    constructor(...args) {
        super(HttpClientErrorStatusEnum.BAD_REQUEST, args[0], args[1]);
    }
}

module.exports = HttpBadRequestException;
