const Exception = require("@enterprize/exceptions").Exception;
const IllegalArgumentException = require("@enterprize/exceptions").IllegalArgumentException;

const HttpStatusConvenienceMethods = require("../status/http-status-convenience-methods.class");

/**
 * Base class for HTTP exceptions.
 * ###
 * Generic Types:
 * - ``T`` - (optional) The type of the details
 *
 * @since 02/03/2020
 */
class HttpException extends Exception {

    //#region Public Attributes
    /**
     * HTTP status code. Must be a 4xx or a 5xx.
     */
    status;
    /**
     * HTTP legible status name.
     * @type string
     */
    statusName;
    //#endregion

    //#region Constructor
    constructor(status, message, details) {

        if (status < 400 || status > 500) {
            throw new IllegalArgumentException("The HTTP \"status\" for errors must be of class 4xx or 5xx", "status");
        }

        super(message, details);

        this.status = status;
        this.statusName = HttpStatusConvenienceMethods.getStatusName(status);
    }
    //#endregion

    //#region Public Methods
    /**
     * Customizes the JSON serialization. Called automatically by JSON.stringfy.
     */
    toJSON() {

        return {
            status: this.status,
            statusName: this.statusName != null ? this.statusName : "",
            message: this.message,
            details: this.details
        };
    }
    //#endregion
}

module.exports = HttpException;
