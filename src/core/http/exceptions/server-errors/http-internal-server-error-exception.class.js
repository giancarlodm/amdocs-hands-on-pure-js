const HttpServerErrorStatusEnum = require("../../status/http-server-error-status.enum");
const HttpException = require("../http-exception.class");

/**
 * The 500 Internal Server Error exceptiom. If details is an instance of {@link Error}, its stack
 * will be serialized.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
class HttpInternalServerErrorException extends HttpException {

    /**
     * constructor();
     * constructor(message: string);
     * constructor(message: string, details: T)
     */
    constructor(...args) {
        super(HttpServerErrorStatusEnum.INTERNAL_SERVER_ERROR, args[0], args[1]);
    }

    /**
     * @inheritDoc
     * Adds the stack trace if {@link details} is an instace of {@link Error}
     */
    toJSON() {

        if (this.details != null && this.details instanceof Error) {

            return {
                ...super.toJSON(),
                details: {
                    ...this.details,
                    name: this.details.name,
                    message: this.details.message,
                    stack: this.details.stack
                }
            };
        }
        else {
            return super.toJSON();
        }
    }
}

module.exports = HttpInternalServerErrorException;
