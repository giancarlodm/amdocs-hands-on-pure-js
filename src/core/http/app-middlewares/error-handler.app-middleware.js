const HttpException = require("../exceptions/http-exception.class");
const HttpInternalServerErrorException = require("../exceptions/server-errors/http-internal-server-error-exception.class");
const HttpServerErrorStatusEnum = require("../status/http-server-error-status.enum");

/**
 * Application Middleware for error treatment. Treats errors generated during requests, returning an
 * appropriate message to the client.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
function errorHandlerAppMiddleware(error, req, res, next) {

    // If the exceptions is an HttpException, return the exception with its code
    if (error instanceof HttpException) {

        res.status(error.status).json(error);
    }
    // Else, 500 Internal Server Error
    else {

        const internalServerErrorException = new HttpInternalServerErrorException("Oops! An server error occurred", {
            name: error.name,
            message: error.message
        });

        res.status(HttpServerErrorStatusEnum.INTERNAL_SERVER_ERROR).json(internalServerErrorException);

        console.error(error);
    }
}

module.exports = errorHandlerAppMiddleware;
