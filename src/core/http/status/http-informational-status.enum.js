/**
 * HTTP informational (1xx) codes enum.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
const HttpInformationalStatusEnum = {

    CONTINUE: 100,
    SWITCHING_PROTOCOLS: 101,
    PROCESSING: 102,
    EARLY_HINTS: 103
};

module.exports = HttpInformationalStatusEnum;
