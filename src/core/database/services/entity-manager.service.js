/**
 * A mock Entity Manager based on the TypeORM Entity Manager service.
 * We provide it as a singleton, assuming there is only one database. If multiple databases are used,
 * we should use another strategy, such as a context driven DI based on some condition (e.g. API key,
 * user, etc).
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
class EntityManager {

    //#region Private Attributes
    /**
     * Represents a database. A place to store entities. A map of "tables" (entity class) with "rows"
     * (objects identified by an id").
     * @type Map<Function, Map<any, {id: any}>>
     */
    static database = new Map();
    /**
     * Sequence generator. A map of current sequence number of a given entity.
     * @type Map<Function, number>
     */
    static sequences = new Map();
    //#endregion

    //#region IEntityManager Methods
    /**
     * Counts the number of rows of a given entity.
     * ###
     * Generic Types:
     * - ``T`` - The type of the entity. We assume it has an Id just for convenience, a true EM should
     *           determine which class attribute is the Primary Key.
     * @param entityClass The entity class to obtain row count.
     * @param conditions Conditions to apply.
     * @param options Pagination options.
     * @return Row count
     */
    static async count(entityClass, conditions, options) {

        if (options != null && options.limit === 0) {
            return 0;
        }

        const table = await this.getTable(entityClass);

        let count = 0;
        if (conditions != null) {

            if (table.has(conditions.id)) {
                return 1;
            }
            else {

                const conditionsKeys = Reflect.ownKeys(conditions);

                let skipped = 0;
                for (let row of table.values()) {

                    let keep = true;
                    for (let key of conditionsKeys) {

                        const rowValue = Reflect.get(row, key);
                        const conditionValue = Reflect.get(conditions, key);

                        if (rowValue !== conditionValue) {
                            keep = false;
                            break;
                        }
                    }

                    if (keep) {

                        if (options != null && (skipped < options.skip)) {
                            skipped++;
                            continue;
                        }

                        count++;
                    }

                    if (options != null && (count >= options.limit)) {
                        return count;
                    }
                }

                return count;
            }
        }

        return table.size;
    }

    /**
     * Retrieves a list of entities.
     * ###
     * Generic Types:
     * - ``T`` - The type of the entity. We assume it has an Id just for convenience, a true EM should
     *           determine which class attribute is the Primary Key.
     * @param entityClass The entity class to obtain row count.
     * @param conditions Conditions to apply.
     * @param options Pagination options.
     * @return List of entities
     */
    static async find(entityClass, conditions, options) {

        const table = await this.getTable(entityClass);
        let rows = Array.from(table.values());

        if (conditions != null) {

            const conditionsKeys = Reflect.ownKeys(conditions);

            rows = rows.filter(row => {

                for (let key of conditionsKeys) {

                    const rowValue = Reflect.get(row, key);
                    const conditionValue = Reflect.get(conditions, key);

                    if (rowValue !== conditionValue) {
                        return false;
                    }
                }

                return true;
            });
        }

        if (options != null) {

            rows.splice(0, options.skip);
            rows.splice(options.limit);
        }

        return rows;
    }

    /**
     * Persists an entity. Performs insertion or update. Setting an sequence generated ID, if necessary,
     * mutating the object.
     * ###
     * Generic Types:
     * - ``T`` - The type of the entity. We assume it has an Id just for convenience, a true EM should
     *           determine which class attribute is the Primary Key.
     * @param entity The entity to persist
     * @return The saved entity. Same object reference as the
     */
    static async save(entity) {

        const constructor = entity.constructor;
        const table = await this.getTable(constructor);

        // its insertion (we allow null IDs)
        if (entity.id === undefined || !table.has(entity.id)) {
            entity.id = this.getSequence(constructor);
            table.set(entity.id, entity);
        }
        else {
            table.set(entity.id, entity);
        }

        return entity;
    }

    /**
     * Deletes an entity.
     * - ``T`` - The type of the entity. We assume it has an Id just for convenience, a true EM should
     *           determine which class attribute is the Primary Key.
     * @param entityClass The class of the entity to delete
     * @param conditions Delete conditions.
     * @return Number of deleted rows
     */
    static async delete(entityClass, conditions) {

        const table = await this.getTable(entityClass);

        if (conditions.id !== undefined && !table.has(conditions.id)) {
            return {removed: 0};
        }

        const conditionsKeys = Reflect.ownKeys(conditions);
        if (conditionsKeys.includes("id") && conditionsKeys.length === 1) {

            table.delete(conditions.id);
            return {removed: 1};
        }
        else {

            let removed = 0;
            for (let row of table.entries()) {

                let removeFlag = true;
                for (let key of conditionsKeys) {

                    const rowValue = Reflect.get(row[1], key);
                    const conditionValue = Reflect.get(conditions, key);

                    if (rowValue !== conditionValue) {
                        removeFlag = false;
                        break;
                    }
                }

                if (removeFlag) {
                    table.delete(row[0]);
                    removed++;
                }
            }

            return {removed: removed};
        }

    }

    /**
     * Updates an entity. Differs from {@link save} in a sense that a partial entity can be used,
     * does not perform insertions and can update multiples rows.
     * - ``T`` - The type of the entity. We assume it has an Id just for convenience, a true EM should
     *           determine which class attribute is the Primary Key.
     * @param entityClass The class of the entity to update
     * @param conditions Update conditions.
     * @param update The update to perform.
     * @return Number of modified rows
     */
    static async update(entityClass, conditions, update) {

        const table = await this.getTable(entityClass);

        if (conditions.id !== undefined && !table.has(conditions.id)) {
            return {modified: 0};
        }

        const conditionsKeys = Reflect.ownKeys(conditions);
        if (conditionsKeys.includes("id") && conditionsKeys.length === 1) {

            const row = table.get(conditions.id);
            const updateKeys = Reflect.ownKeys(update);
            for (let key of updateKeys) {
                Reflect.set(row, key, Reflect.get(update, key));
            }

            return {modified: 1};
        }
        else {

            let modified = 0;
            for (let row of table.values()) {

                let updateFlag = true;
                for (let key of conditionsKeys) {

                    const rowValue = Reflect.get(row, key);
                    const conditionValue = Reflect.get(conditions, key);

                    if (rowValue !== conditionValue) {
                        updateFlag = false;
                        break;
                    }
                }

                if (updateFlag) {

                    const updateKeys = Reflect.ownKeys(update);
                    for (let key of updateKeys) {
                        Reflect.set(row, key, Reflect.get(update, key));
                    }

                    modified++;
                }
            }

            return {modified: modified};
        }
    }
    //#endregion

    //#region Private Methods
    /**
     * @private
     * Obtain the row map of a given entity. Creates the map if not yet defined.
     * @param entityClass The entity Class
     * @return {Map} Row Map of the entity class.
     */
    static getTable(entityClass) {

        if (!this.database.has(entityClass)) {
            this.database.set(entityClass, new Map());
            this.sequences.set(entityClass, 1);
        }

        return Promise.resolve(this.database.get(entityClass));
    }

    /**
     * @private
     * Generates a sequence for a given entity class
     * @param entityClass The entity class to generate the next value.
     * @return {number} The generated sequence value.
     */
    static getSequence(entityClass) {

        const current = this.sequences.get(entityClass);
        this.sequences.set(entityClass, current + 1);

        return current;
    }
    //#endregion
}

module.exports = EntityManager;
