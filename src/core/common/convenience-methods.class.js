/**
 * Static class with convenience methods.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
class ConvenienceMethods {

    /**
     * Retrieve an enumerator values as an Array.
     * ###
     * Generic Types:
     * - ``C`` - The enumerator type.
     * - ``M`` - The type of the enum value.
     * @param enumerator The enumerator to retrieve values.
     * @returns {Array} The enumerator values.
     */
    static getEnumeratorValues(enumerator) {

        const values = [];
        const keys = Reflect.ownKeys(enumerator);
        for (let key of keys) {

            if (!Number.isNaN(Number(key))) {
                continue;
            }

            values.push(Reflect.get(enumerator, key));
        }

        return values;
    }
}

module.exports = ConvenienceMethods;
