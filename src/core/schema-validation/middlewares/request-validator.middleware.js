const HttpBadRequestException = require("../../http/exceptions/client-errors/http-bad-request-exception.class");
const jsonSchemaValidatorService = require("../services/json-schema-validation.service");

/**
 * Request validation middleware factory.
 * @param requestSchema {{headers?: Object, params?: Object, query?: Object, body?: Object}}
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
function requestValidatorMiddlewareFactory(requestSchema) {

	return async (req, res, next) => {

		if (requestSchema.headers != null) {

			const validateFunction = await jsonSchemaValidatorService.compile(requestSchema.headers);
			if (!validateFunction(req.headers)) {
				next(new HttpBadRequestException("The request \"headers\" did not pass JSON Schema validation.", validateFunction.errors));
				return;
			}
		}

		if (requestSchema.params != null) {

			const validateFunction = await jsonSchemaValidatorService.compile(requestSchema.params);
			if (!validateFunction(req.params)) {
				next(new HttpBadRequestException("The request \"URL parameters\" did not pass JSON Schema validation.", validateFunction.errors));
				return;
			}
		}

		if (requestSchema.query != null) {

			const validateFunction = await jsonSchemaValidatorService.compile(requestSchema.query);
			if (!validateFunction(req.query)) {
				next(new HttpBadRequestException("The request \"query parameters\" did not pass JSON Schema validation.", validateFunction.errors));
				return;
			}
		}

		if (requestSchema.body != null) {

			const validateFunction = await jsonSchemaValidatorService.compile(requestSchema.body);
			if (!validateFunction(req.body)) {
				next(new HttpBadRequestException("The request \"body\" did not pass JSON Schema validation.", validateFunction.errors));
				return;
			}
		}

		next();
	};
}

module.exports = requestValidatorMiddlewareFactory;
