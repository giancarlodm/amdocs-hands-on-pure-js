const Ajv = require("ajv");

/**
 * JSON schema validation service. Uses AJV to perform validation.
 *
 * @sinceVersion 1.0.0
 * @author Giancarlo Dalle Mole
 * @since 02/03/2020
 */
const jsonSchemaValidatorService = new Ajv({
	format: "full",
	useDefaults: true,
	coerceTypes: true,
	loadSchema: () => void 0
});

module.exports = jsonSchemaValidatorService;
